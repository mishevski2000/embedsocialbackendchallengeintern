$( document ).ready(function()
{

    // FETCH METHOD TO CALL AJAX FUNCTION TO PRINT ALL REVIEWS ON PAGE LOAD

    fetch("./reviews.json")
        .then(function(resp) {
            return resp.json();
        })
        .then(function(data) {
            $.ajax({
                url:"./PrintData.php",
                method: "POST",
                data:{
                    data: data, 
                },
                success:function(data){
                    $("tbody").html(data);
                }

            })

            
        });

        //EVENT LISTENERS TO TRACH CHANGES IN FILTERS

        let orderByRating = "desc";
        let minimumRating = 1;
        let prioritizeByText = "yes";
        let orderByDate = "old";

        $("select#order-by-rating").change(function(){
            orderByRating = $(this).children("option:selected").val();
        });


        $("select#minimum-rating").change(function(){
            minimumRating  = $(this).children("option:selected").val();
        });

        $("select#order-by-date").change(function(){
            orderByDate  = $(this).children("option:selected").val();
        });

        $("select#prioritize-by-text").change(function(){
            prioritizeByText  = $(this).children("option:selected").val();
        });

        

        //ON CLICK CALLING AJAX FUNCETION TO PRINT FILTERED AND ORDERED REVIEWS

        $("#filter").click(function()
        {
            fetch("./reviews.json")
                .then(function(resp) {
                    return resp.json();
                })
                .then(function(data) {
                    $.ajax({
                        url: "./filterData.php",
                        method: "POST",
                        data:{
                            data: data,
                            oderByRating: orderByRating,
                            minimumRating: minimumRating,
                            prioritizeByText: prioritizeByText, 
                            orderByDate: orderByDate
                        },
                        success:function(data){
                            $("tbody").html(data);
                        }

                    })

                    
                });
        })
});