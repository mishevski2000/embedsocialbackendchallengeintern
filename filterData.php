<?php

require_once "./functions.php";

//PULLING ALL VARIABLES FORM AJAX CALL


$data = $_POST['data']; //GETTING DATA FORM CALL
$prioritizeByText = $_POST['prioritizeByText']; //GETTING YES/NO STRING TO PRIORITIZE BY TEXT
$orderByRating = $_POST['oderByRating']; //GETTING DESC/ASC STRING TO ORDER BY RATING
$orderByDate = $_POST['orderByDate']; //GETTING DESC/ASC STRING TO ORDER BY DATE

$data = array_filter($data, function($review) //Funcetion to filter the array from reviews the user doesnt want to show
{
    $minimumRating = $_POST['minimumRating']; //GETTING MINIMUM RATING FOR FILTERING REVIEWS

    return $review['rating'] >= $minimumRating; //Returning only reviews where there rating is equel or greater than minimum rating requirement
});

if($prioritizeByText === "yes") //If statement for where the user wanted to priortiteze the reviews by text
{
    if($orderByRating === "desc") // if statement for where the user wanted to order by rating desc
    {
        if($orderByDate === "new") // if statement for where the user wanted to order by date from newest
        {
            usort($data, function ($review_one, $review_two){   
                return [$review_two['reviewText'] != "", $review_two['rating'], $review_two['reviewCreatedOnDate']]
                        >
                        [$review_one['reviewText'] != "", $review_one['rating'], $review_one['reviewCreatedOnDate']];
            });
        }
        elseif($orderByDate === "old") // if statement for where the user wanted to order by date from oldest
        {
            usort($data, function ($review_one, $review_two){   
                return [$review_two['reviewText'] != "", $review_two['rating'], $review_one['reviewCreatedOnDate']]
                        >
                        [$review_one['reviewText'] != "", $review_one['rating'], $review_two['reviewCreatedOnDate']];
            });  
        }
    }
    elseif($orderByRating === "asc") // if statement for where the user wanted to order by rating asc
    {
        if($orderByDate === "new") // if statement for where the user wanted to order by date from newest
        {
            usort($data, function ($review_one, $review_two){   
                return [$review_two['reviewText'] != "", $review_one['rating'], $review_two['reviewCreatedOnDate']]
                        >
                        [$review_one['reviewText'] != "", $review_two['rating'], $review_one['reviewCreatedOnDate']];
            });
        }
        elseif($orderByDate === "old") // if statement for where the user wanted to order by date from oldest
        { 
            usort($data, function ($review_one, $review_two){   
                return [$review_two['reviewText'] != "", $review_one['rating'], $review_one['reviewCreatedOnDate']]
                        >
                        [$review_one['reviewText'] != "", $review_two['rating'], $review_two['reviewCreatedOnDate']];
            });  
        }
    }
}
elseif($prioritizeByText === "no")
{
    if($orderByRating === "desc") // if statement for where the user wanted to order by rating desc
    { 
        if($orderByDate === "new") // if statement for where the user wanted to order by date from newest
        {
            usort($data, function ($review_one, $review_two){   
                return [$review_two['rating'], $review_two['reviewCreatedOnDate']]
                        >
                        [$review_one['rating'], $review_one['reviewCreatedOnDate']];
            });
        }
        elseif($orderByDate === "old") // if statement for where the user wanted to order by date from oldest
        {
            usort($data, function ($review_one, $review_two){   
                return [$review_two['rating'], $review_one['reviewCreatedOnDate']]
                        >
                        [$review_one['rating'], $review_two['reviewCreatedOnDate']];
            });  
        }
    }
    elseif($orderByRating === "asc") // if statement for where the user wanted to order by rating asc
    {
        if($orderByDate === "new") // if statement for where the user wanted to order by date from newest
        {
            usort($data, function ($review_one, $review_two){   
                return [$review_one['rating'], $review_two['reviewCreatedOnDate']]
                        >
                        [$review_two['rating'], $review_one['reviewCreatedOnDate']];
            });
        }
        elseif($orderByDate === "old") // if statement for where the user wanted to order by date from newest
        {
            usort($data, function ($review_one, $review_two){   
                return [$review_one['rating'], $review_one['reviewCreatedOnDate']]
                        >
                        [$review_two['rating'], $review_two['reviewCreatedOnDate']];
            });  
        }
    }
}

foreach($data as $review)// Printing the reviews
{
        PrintReview($review); 
}

?>